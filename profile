export PATH="/opt/local/libexec/gnubin:/opt/local/bin:/opt/local/sbin:$PATH:$HOME/bin"
export MANPATH="/opt/local/share/man:$MANPATH"

cool_prompt() {
    local EXIT="$?"
    local BOLD="\[\e[1m\]"
    local RESET="\[\e[0m\]"
    local RED="\[\e[31m\]"
    local GREEN="\[\e[32m\]"
    local YELLOW="\[\e[33m\]"
    local BLUE="\[\e[34m\]"
    local MAGENTA="\[\e[35m\]"
    local CYAN="\[\e[36m\]"
    local GRAY="\[\e[37m\]"
    local OPEN="$GREEN["
    local CLOSE="$GREEN]"
    PS1="$OPEN$CYAN\u$GRAY@$BOLD\h$RESET$CLOSE$OPEN$YELLOW$(pwd)$CLOSE$OPEN$BOLD"
    if test $EXIT -eq 0; then
	PS1="$PS1${GREEN}✔$RESET$CLOSE$RESET:~ "
    else
	PS1="$PS1${RED}✘$RESET$CLOSE$RESET:~ "
    fi
}

export PROMPT_COMMAND=cool_prompt
if which gls >/dev/null; then
    alias ls="gls -Fa --color=auto"
else
    alias ls="ls -FaG"
fi

export EDITOR=emacs
export CLICOLOR=1
